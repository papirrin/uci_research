source('mecdf/mecdf.R')

create.support.points  <- function(a.idxs, b.idxs, s_by=1, mode='common'){
  # idxs must be sorted
  s_start <- 0
  s_end <- 0
  if(mode=='common'){
    s_start <- max(head(a.idxs, n=1), head(b.idxs, n=1))
    s_end <- min(tail(a.idxs, n=1), tail(b.idxs, n=1))
  }
  else if(mode=='union'){
    s_start <- min(head(a.idxs, n=1), head(b.idxs, n=1))
    s_end <- max(tail(a.idxs, n=1), tail(b.idxs, n=1))
    }
  else{
    stop(paste('Invalid mode value', mode))
  }
  
  e_range <- seq(s_start, s_end, by=s_by)
  # Append last regardles of step
  if(tail(e_range,n=1) != s_end){
    e_range <- c(e_range, s_end)
  }
  return(e_range)
}

calc.dist <- function(a,b,mode='L1'){
  if(mode=='L1'){
    return(sum(abs(b-a)))
  }
  else{
    stop(paste('Invalid mode value', mode))
  }
}

calc.dist(c(1,3,5),c(1,-2,8))

#ks.test(c(2,5,8,5,5,5),c(3,5,5,9,10,10))

#i_this <- calcObsIndexes(one)
#i_other <- calcObsIndexes(other)

#create.support.points(1:10, c(-1:4,8:12), mode='union', s_by=3)
#create.support.points(1:10, c(-1:4,8:12), mode='common', s_by=3)