source('mecdf/mecdf.R')
source('mecdf/tests/helpers.R')

library('RUnit')


test.calculateECDF.linear.one <- function(){
  # TODO: from -1 to 6
  obs <- create_obs()
  nr_obs <- length(obs)
  
  # -1
  gen <- create_gen(obs)
  v_i <- c(-1)
  cur <- calculateECDF.linear(gen, v_i)
  ex <- data.frame(var_index=v_i,obs=as.numeric(c(NA)), row.names=paste('var', v_i, sep='='))
  checkEquals(ex, cur, checknames=FALSE)
  
  gen <- create_gen(obs)
  v_i <- c(0)
  cur <- calculateECDF.linear(gen, v_i)
  ex <- data.frame(var_index=v_i,obs=as.numeric(c(14.5)), row.names=paste('var', v_i, sep='='))
  checkEquals(ex, cur, checknames=FALSE)
  
  gen <- create_gen(obs)
  v_i <- c(1)
  cur <- calculateECDF.linear(gen, v_i)
  ex <- data.frame(var_index=v_i,obs=as.numeric(mean(c(12.5,10,11))), row.names=paste('var', v_i, sep='='))
  checkEquals(ex, cur, checknames=FALSE)
  
}

test.calculateECDF.linear.lessthanreps <- function(){
  obs <- create_obs()
  nr_obs <- length(obs)
  gen <- create_gen(obs)
  v_i <- c(0,1)
  cur <- calculateECDF.linear(gen, v_i)
  ex <- data.frame(var_index=v_i,obs=c(14.5,mean(c(10,11,12.5))), row.names=paste('var', v_i, sep='='))
  checkEquals(ex, cur, checknames=FALSE)
}

test.calculateECDF.linear.morethanreps <- function(){
  obs <- create_obs()
  nr_obs <- length(obs)
  gen <- create_gen(obs)
  v_i <- c(-1,0,1,2,3,4,5,6,7,9,12,15)
  cur <- calculateECDF.linear(gen, v_i)
  #checkEqualsNumeric(data.frame(var_index=v_i,obs=c(14.5,mean(10,13,14,14,11,12.5))), cur)
}

test.findObs_long <- function(){
  # TODO: Test with imputation on borders
  obs <- create_obs_long()
  gen <- create_gen(obs)
  cur <- find.observations(gen, c(0,1,2,3,4,5))
  
}