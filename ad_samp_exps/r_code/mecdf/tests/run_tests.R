library ('RUnit')

test.suite <- defineTestSuite('mecdf-test', dirs=file.path(getwd(), "mecdf/tests"))

test.result <- runTestSuite(test.suite)

printTextProtocol(test.result)