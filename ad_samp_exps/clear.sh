#!/bin/bash
set -e

# Clear the folder, except the script files
ls *|grep -v -E 'run_round.sh|clear.sh' | xargs cat

