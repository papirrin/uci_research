#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/*
* Sum iter
*/
struct args_li{
  long int arg;
};

int sum_iter(void * arg){
  struct args_li * targ = (struct args_li *)arg;

  printf("Run sum_iter, nr_iters %ld\n", targ->arg);

  long int i;
  long int total = 0;
  for(i = 0; i < targ->arg; ++i){
    total+= i;
  }
  return 0;
}

int prod_add_loop(void * arg){
  struct args_li * targ = (struct args_li *)arg;
  printf("Run prod_add_loop, nr_iters %ld\n", targ->arg);
	/* Program Starts*/
	long int tot = 0;
	long int i = 0;
	for (i = 0; i < targ->arg; i++) {
          tot *= 3;
          tot += 2;
	}
}

int fpoint_add_loop(void * arg){
  struct args_li * targ = (struct args_li *)arg;
  printf("Run fpoint_add_loop, nr_iters %ld\n", targ->arg);
	/* Program Starts*/
	double sum = 0;
	long int i = 0;
	for (i = 0; i < targ->arg; i++) {
		sum += 2.05;
	}

}

int int_div_sub(void * arg){
  struct args_li * targ = (struct args_li *)arg;
  printf("Run int_div_sub, nr_iters %ld\n", targ->arg);
	/* Program Starts*/
	long int sum = targ->arg * 7;
	long int i = 0;
	for (i = 0; i < targ->arg; i++) {
		sum = (sum - 1) / 7;
	}

}

int it_fibonacci(void * arg){
  struct args_li * targ = (struct args_li *)arg;
  printf("Run it_fibonacci, nr_iters %ld\n", targ->arg);
	int first = 0, second = 1, next, c;
	for (c = 0; c < targ->arg; c++) {
		if (c <= 1)
			next = c;
		else {
			next = first + second;
			first = second;
			second = next;
		}
	}
}
