#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sysexits.h>
#include <string.h>

#include "task_lib.h"

struct task_desc{
  int (*task)(void *);
  void * args;
  struct task_desc * next;
};

struct task_desc* alloc_task(){
  struct task_desc* t = malloc(sizeof(struct task_desc));
  t->task = NULL;
  t->args = NULL;
  t->next = NULL;
};

struct task_desc * create_one_arg_task(int(*task)(void *), long int arg){
  struct task_desc * t = alloc_task();
  struct args_li * args = malloc(sizeof(struct args_li));

  args->arg = arg;
  t->task = task;
  t->args = args;
  return t;  
}

int main(int argc, char * argv[]){
  const char * opt_string = "u:";
  struct task_desc *tasks_head = NULL, *curr_task, *new_task;

  char usage [100];
  char *options=NULL;
  char *one_opt=NULL;
  sprintf(usage, "Usage %s [u tasktype1:arg_values1,tasktype2:arg_values2,...", argv[0]);

  int opt = getopt(argc, argv, opt_string);
  while( opt != -1){
    switch(opt){
      case 'u':
        options = strtok(optarg, ":,");

        while(options!=NULL){

          switch(options[0]){
            case 's':
              new_task = create_one_arg_task(sum_iter, atol(strtok(NULL,":,")));
            break;
            case 'f':
              new_task = create_one_arg_task(it_fibonacci, atol(strtok(NULL,":,")));
            break;
            case 'd':
              new_task = create_one_arg_task(int_div_sub, atol(strtok(NULL,":,")));
            break;
            case 'a':
              new_task = create_one_arg_task(fpoint_add_loop, atol(strtok(NULL,":,")));
            break;
            default:
              fprintf(stderr, "Invalid task option %c\n", options[0]);
              exit(-1);
            break;

          }
          if(tasks_head == NULL){
            curr_task = tasks_head = new_task;
          }
          else{
            curr_task = curr_task->next= new_task;
          }
          options = strtok(NULL, ":,");
        }
        break;
       
    default:
      printf("%s\n",usage);
      exit(-EX_USAGE);
    }
    opt = getopt(argc, argv, opt_string);
  }

  printf("Load PID: %d\n", getpid());

  // Execute all tasks in list
  struct task_desc * t = tasks_head;
  while(t != NULL){
    t->task(t->args);
    t = t->next;
  }

  // Free task list
  struct task_desc * tp = tasks_head, *tc;

  while(tp != NULL){
    tc=tp->next;

    if(tp->args != NULL){
      free(tp->args);
    }
    free(tp);
    tp=tc;
  }
  return 0;
}


