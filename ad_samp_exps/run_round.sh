#!/bin/bash
set -e
set -u

readonly spe_root=~/uci_research/spe/src
readonly spe_cmd=spe

#remake=${1:-Y}
#nb_iters=${2:-5}
#output_preffix=${3:-test}

# TODO: If the number of argumens before the flags is variable, how do we get the flags? hence no default values
remake=$1
nb_iters=$2
output_preffix=$3
script_rand=$4
if [ $# -lt 4 ]
then
  echo "Usage: ${spe_cmd} remake nb_iters output_preffix script_rand flags_and_command"
  exit -1
fi
shift 4
flags=${@:--r 5,50000,500000 ../loads/tasks_cmd -u s:50000000}

echo "Copying files"

rm -rf bin
rm -rf data

mkdir bin
mkdir data

if [ "$remake" == "Y" ]
then
  make -C ${spe_root} clean all
fi

if [ "$script_rand" != "N" ]
then
  OIFS=$IFS
  IFS=','
  r_arr=($script_rand)
  RANDOM=${r_arr[0]}
  r_min=${r_arr[1]}
  r_max=${r_arr[2]}
  r_range=$r_max-$r_min
  IFS=$OIFS
fi

cp ${spe_root}/${spe_cmd} ./bin/

summ_file="./data/${output_preffix}_summary.csv"
echo Summary file:$summ_file
echo "run,run_name,summary_fn" > $summ_file

for ((  i = 1 ;  i <= nb_iters;  i++  ))
do
  iter_flags=$flags
  iter_output_pre="${output_preffix}_${i}"
  if [ "$script_rand" != "N" ]
  then
    r_val=$((r_min+$RANDOM*r_range/32767))
    iter_flags="-p ${r_val} ${iter_flags}"
  fi

  echo ${i},${iter_output_pre},${iter_output_pre}_summary.csv >> $summ_file

  cmd="../bin/${spe_cmd} -o ${iter_output_pre} ${iter_flags}"
  echo "Running \"$cmd\" for the ${i}th time:"
  
  (cd data && exec $cmd)

done

general_cmd="../bin/${spe_cmd} -o ${output_preffix} ${flags}"
echo "Run \"$general_cmd\" ${nb_iters} times."
