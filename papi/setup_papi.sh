#!/bin/bash
set -o errexit
set -o nounset

PAPI_BUILD=papi-5.0.1

# Install PAPI
wget http://icl.cs.utk.edu/projects/papi/downloads/$PAPI_BUILD.tar.gz

tar -zxf $PAPI_BUILD.tar.gz 

cd $PAPI_BUILD/src

./configure
make
make fulltest
sudo make install-all

mkdir ../../src
cd ../../src

#
# Create handle_error.c
#
cat <<EOF >handle_error.c
#include <stdlib.h>
#include <stdio.h>
#include <papi.h>

void handle_error (int retval)
{
     printf("PAPI error %d: %s\n", retval, PAPI_strerror(retval));
     exit(1);
}
EOF

#
# Create example.c
#
cat <<EOF >example.c
#include <papi.h>
#include <stdio.h>
main()
{
int Events[2] = { PAPI_TOT_CYC, PAPI_TOT_INS };
int num_hwcntrs = 0;

/* Initialize the PAPI library and get the number of counters available */
if ((num_hwcntrs = PAPI_num_counters()) <= PAPI_OK)  
    handle_error(1);

printf("This system has %d available counters.", num_hwcntrs);

if (num_hwcntrs > 2)
    num_hwcntrs = 2;

     /* Start counting events */
     if (PAPI_start_counters(Events, num_hwcntrs) != PAPI_OK)
         handle_error(1);
}
EOF

gcc -I../$PAPI_BUILD/src -c handle_error.c

echo '##############################'
echo '##############################'
echo '##############################'
echo 'Change to src/ to compile examples'
echo 'To compile example.c, type (on src/):'
echo 'gcc -I../'$PAPI_BUILD'/src handle_error.o example.c ../'$PAPI_BUILD'/src/libpapi.a -o example
'

echo 'To run example.c, type (on src/):'
echo './example'
