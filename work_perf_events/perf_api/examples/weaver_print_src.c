/* print_record_sample.c  */
/* by Vince Weaver   vincent.weaver _at_ maine.edu */

/* This just tests perf_event sampling */

#define _GNU_SOURCE 1

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <fcntl.h>

#include <errno.h>

#include <signal.h>

#include <sys/mman.h>

#include <sys/ioctl.h>
#include <asm/unistd.h>
#include <sys/prctl.h>
#include "../src/pe_sampling.c"

#define SAMPLE_FREQUENCY 100000

#define MMAP_DATA_SIZE 8

int sample_type=PERF_SAMPLE_IP | PERF_SAMPLE_TID | PERF_SAMPLE_TIME |
                  PERF_SAMPLE_ADDR | PERF_SAMPLE_READ | PERF_SAMPLE_CALLCHAIN |
                  PERF_SAMPLE_ID | PERF_SAMPLE_CPU | PERF_SAMPLE_PERIOD |
                  PERF_SAMPLE_STREAM_ID | PERF_SAMPLE_RAW ;
   //                  PERF_SAMPLE_BRANCH_STACK;


int read_format=
                PERF_FORMAT_GROUP |
                PERF_FORMAT_ID |
                PERF_FORMAT_TOTAL_TIME_ENABLED |
                PERF_FORMAT_TOTAL_TIME_RUNNING;


int quiet=0;

static struct signal_counts {
  int in,out,msg,err,pri,hup,unknown,total;
} count = {0,0,0,0,0,0,0,0};

static int fd1,fd2;

void *our_mmap;

long long prev_head=0;

static void our_handler(int signum,siginfo_t *oh, void *blah) {
  int ret;

  ret=ioctl(fd1, PERF_EVENT_IOC_DISABLE, quiet);

  switch(oh->si_code) {
     case POLL_IN:  count.in++;  break;
     case POLL_OUT: count.out++; break;
     case POLL_MSG: count.msg++; break;
     case POLL_ERR: count.err++; break;
     case POLL_PRI: count.pri++; break;
     case POLL_HUP: count.hup++; break;
     default: count.unknown++; break;
  }

  count.total++;

  ret=ioctl(fd1, PERF_EVENT_IOC_REFRESH, 1);

  (void) ret;
  
}

int instructions_million(void) {

#if defined(__i386__) || (defined __x86_64__)   
   asm("\txor %%ecx,%%ecx\n"
       "\tmov $499999,%%ecx\n"
       "test_loop:\n"
       "\tdec %%ecx\n"
       "\tjnz test_loop\n"
       : /* no output registers */
       : /* no inputs */
       : "cc", "%ecx" /* clobbered */
    );
    return 0;
#elif defined(__PPC__)
   asm("\tnop                           # to give us an even million\n"
       "\tlis     15,499997@ha          # load high 16-bits of counter\n"
       "\taddi    15,15,499997@l        # load low 16-bits of counter\n"
       "55:\n"
       "\taddic.  15,15,-1              # decrement counter\n"
       "\tbne     0,55b                  # loop until zero\n"
       : /* no output registers */
       : /* no inputs */
       : "cc", "15" /* clobbered */
    );
    return 0;
#elif defined(__ia64__)
   asm("\tmov     loc6=166666           // below is 6 instr.\n"
       ";;                              // because of that we count 4 too few\n"
       "55:\n"
       "\tadd     loc6=-1,loc6          // decrement count\n"
       ";;\n"
       "\tcmp.ne  p2,p3=0,loc6\n"
       "(p2)    br.cond.dptk    55b     // if not zero, loop\n"
       : /* no output registers */
       : /* no inputs */
       : "p2", "loc6" /* clobbered */
    );
    return 0;
#elif defined(__sparc__)
   asm("\tsethi     %%hi(333333), %%l0\n"
       "\tor        %%l0,%%lo(333333),%%l0\n"
       "test_loop:\n"
       "\tdeccc   %%l0             ! decrement count\n"
       "\tbnz     test_loop        ! repeat until zero\n"
       "\tnop                      ! branch delay slot\n"
       : /* no output registers */
       : /* no inputs */
       : "cc", "l0" /* clobbered */
    );
    return 0;
#elif defined(__arm__)
    asm("\tldr     r2,count                    @ set count\n"
        "\tb       test_loop\n"
        "count:       .word 333332\n"
        "test_loop:\n"
        "\tadd     r2,r2,#-1\n"
        "\tcmp     r2,#0\n"
        "\tbne     test_loop                    @ repeat till zero\n"
       : /* no output registers */
       : /* no inputs */
       : "cc", "r2" /* clobbered */
	);

    return 0;

#endif
   
    return 0;//CODE_UNIMPLEMENTED;

}

int main(int argc, char** argv) {
   
   int ret;
   int mmap_pages=1+MMAP_DATA_SIZE;

   struct perf_event_attr pe;

   struct sigaction sa;
   char test_string[]="Testing record sampling...";
   
//   quiet=test_quiet();

   if (!quiet) printf("This tests the record sampling interface.\n");
   
   memset(&sa, 0, sizeof(struct sigaction));
   sa.sa_sigaction = our_handler;
   sa.sa_flags = SA_SIGINFO;

   if (sigaction( SIGIO, &sa, NULL) < 0) {
     fprintf(stderr,"Error setting up signal handler\n");
     exit(1);
   }
   
   memset(&pe,0,sizeof(struct perf_event_attr));

   pe.type=PERF_TYPE_HARDWARE;
   pe.size=sizeof(struct perf_event_attr);
   pe.config=PERF_COUNT_HW_INSTRUCTIONS;
   pe.sample_period=SAMPLE_FREQUENCY;
   pe.sample_type=sample_type;

   pe.read_format=read_format;
   pe.disabled=1;
   pe.pinned=1;
   pe.exclude_kernel=1;
   pe.exclude_hv=1;
   pe.wakeup_events=1;

//   arch_adjust_domain(&pe,quiet);

   fd1=sys_perf_event_open(&pe,0,-1,-1,0);
   if (fd1<0) {
     if (!quiet) fprintf(stderr,"Error opening leader %llx\n",pe.config);
   //  test_fail(test_string);
   }

   memset(&pe,0,sizeof(struct perf_event_attr));

   pe.type=PERF_TYPE_HARDWARE;
   pe.size=sizeof(struct perf_event_attr);
   pe.config=PERF_COUNT_HW_CPU_CYCLES;
   pe.sample_type=PERF_SAMPLE_IP;
   pe.read_format=PERF_FORMAT_GROUP|PERF_FORMAT_ID;
   pe.disabled=0;
   pe.exclude_kernel=1;
   pe.exclude_hv=1;

//   arch_adjust_domain(&pe,quiet);

   fd2=sys_perf_event_open(&pe,0,-1,fd1,0);
   if (fd2<0) {
     if (!quiet) fprintf(stderr,"Error opening %llx\n",pe.config);
   //  test_fail(test_string);
   }

   our_mmap=mmap(NULL, mmap_pages*4096, 
                 PROT_READ|PROT_WRITE, MAP_SHARED, fd1, 0);

   
   fcntl(fd1, F_SETFL, O_RDWR|O_NONBLOCK|O_ASYNC);
   fcntl(fd1, F_SETSIG, SIGIO);
   fcntl(fd1, F_SETOWN,getpid());
   
   ioctl(fd1, PERF_EVENT_IOC_RESET, 0);   

   ret=ioctl(fd1, PERF_EVENT_IOC_ENABLE,0);

   if (ret<0) {
     if (!quiet) fprintf(stderr,"Error with PERF_EVENT_IOC_ENABLE of group leader: "
	     "%d %s\n",errno,strerror(errno));
     exit(1);
   }

   instructions_million();
  
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
printf("hola estoy imprimiendo mucho\n");
int i=0;
int sum=0;
for(;i<100000000;i++){
  sum += 2;
}
printf("hola estoy imprimiendo mucho\n");
   
   ret=ioctl(fd1, PERF_EVENT_IOC_REFRESH,0);

   if (!quiet) {
      printf("Counts, using mmap buffer %p\n",our_mmap);
      printf("\tPOLL_IN : %d\n",count.in);
      printf("\tPOLL_OUT: %d\n",count.out);
      printf("\tPOLL_MSG: %d\n",count.msg);
      printf("\tPOLL_ERR: %d\n",count.err);
      printf("\tPOLL_PRI: %d\n",count.pri);
      printf("\tPOLL_HUP: %d\n",count.hup);
      printf("\tUNKNOWN : %d\n",count.unknown);
   }

   if (count.total==0) {
      if (!quiet) printf("No overflow events generated.\n");
//      test_fail(test_string);
   }

   munmap(our_mmap,mmap_pages*4096);

   close(fd2);
   close(fd1);

//   test_pass(test_string);
   
   return 0;
}
