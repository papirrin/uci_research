#!/bin/sh
set -e

in_name=$1_src.c
out_name=$1_out.o
echo "Compiling ${in_name}\n***********************************"
gcc -o $out_name $in_name

echo "Running ${out_name}\n***********************************" 

./$out_name
