#include "../src/pe_perf_events.c"

int main (int argc, char **argv){
  pe_context_t ctx;
  ctx.initialized = 0;
  ctx.state = 0;

  pe_control_t ctl;
  ctl.num_events = 1;
  ctl.num_domain = 1;
  ctl.granularity = 1;
  :q
  int ret = open_pe_events(&ctx, &ctl);

  if(ret != PAPI_OK){
    PAPIERROR( "Unable to open events");
  }
  printf("Events opened\n");

  
}
