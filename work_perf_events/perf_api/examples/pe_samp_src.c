#include "../src/pe_sampling.c"
#define MMAP_DATA_SIZE 4

int sample_type=PERF_SAMPLE_IP | PERF_SAMPLE_TID | PERF_SAMPLE_TIME |
                  PERF_SAMPLE_ADDR | PERF_SAMPLE_READ | PERF_SAMPLE_CALLCHAIN |
                  PERF_SAMPLE_ID | PERF_SAMPLE_CPU | PERF_SAMPLE_PERIOD |
                  PERF_SAMPLE_STREAM_ID | PERF_SAMPLE_RAW ;
   //                  PERF_SAMPLE_BRANCH_STACK

int read_format=
                PERF_FORMAT_GROUP |
                PERF_FORMAT_ID |
                PERF_FORMAT_TOTAL_TIME_ENABLED |
                PERF_FORMAT_TOTAL_TIME_RUNNING;

#include "../src/pe_process_sample.c"

void load(long int nb_loops){

  /* Program Starts*/
  long int sum = 0;
  long int i = 0;
  for(i = 0; i < nb_loops; i++){
    sum += 2;
  }

}

int
main(int argc, char **argv)
{
  int ret, fd = -1;


  prepare_pe();
  int error = PE_OK;

  pe_control_t ctl;

  ctl.cpu = -1;
  ctl.tid = 0;
  ctl.num_events = 1;

  ctl.events[0].group_leader_fd = -1;
  ctl.events[0].event_fd = -1;
  ctl.events[0].event_opened = 0;
  /* How many pages do we really need */
  ctl.events[0].nr_mmap_pages = 1 + MMAP_DATA_SIZE;
 
  struct perf_event_attr * attr = &(ctl.events[0].attr);
  
  memset(attr, 0, sizeof(struct perf_event_attr));
  attr->type = PERF_TYPE_HARDWARE;
  attr->size = sizeof(struct perf_event_attr);
  attr->config = PERF_COUNT_HW_INSTRUCTIONS;
  
  attr->sample_period = 100000;
  attr->sample_type = sample_type;
  attr->read_format = read_format;
  
  attr->disabled = 1;
  attr->pinned = 1;
  attr->exclude_kernel = 1;
  attr->exclude_hv = 1;

  attr->exclude_user=0;
  attr->exclude_kernel=0;
  attr->exclude_hv=0;

  attr->wakeup_events = 100;


  error = open_pe_events(&ctl);
  if (error != PE_OK) {
    fprintf(stderr, "Error opening event\n");
    exit(EXIT_FAILURE);
  }

  our_mmap = ctl.events[0].mmap_buf;

  fd = ctl.events[0].event_fd;
  printf("Opening fd %d\n", fd);

  ioctl(fd, PERF_EVENT_IOC_RESET, 0);
  ret = ioctl(fd, PERF_EVENT_IOC_ENABLE, 0);

  if (ret<0) {
     fprintf(stderr,"Error with PERF_EVENT_IOC_ENABLE of group leader: "
	     "%d %s\n",errno,strerror(errno));
     exit(1);
  }

//  printf("Version: %u, lock: %u, offset: %u, time_enabled: %u, time_running: %u, cap_usr_time %u, cap_usr_rdpmc: %u, \n", );

  load(1000000000);

//ioctl(fd, PERF_EVENT_IOC_REFRESH,0);
  ioctl(fd, PERF_EVENT_IOC_DISABLE, 0);

  error =  close_pe_events(&ctl);
  if (error != PE_OK) {
    fprintf(stderr, "Error closing event\n");
    exit(EXIT_FAILURE);
  }


      printf("Counts, using mmap buffer %p\n",our_mmap);
      printf("\tPOLL_IN : %d\n",count.in);
      printf("\tPOLL_OUT: %d\n",count.out);
      printf("\tPOLL_MSG: %d\n",count.msg);
      printf("\tPOLL_ERR: %d\n",count.err);
      printf("\tPOLL_PRI: %d\n",count.pri);
      printf("\tPOLL_HUP: %d\n",count.hup);
      printf("\tUNKNOWN : %d\n",count.unknown);

   if (count.total==0) {
      if (!quiet) printf("No overflow events generated.\n");
      else
         printf("%d signals were received\n", count.total);
   }
}

