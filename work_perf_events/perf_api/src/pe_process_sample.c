#include "parse_record.c"


static struct signal_counts {
  int in,out,msg,err,pri,hup,unknown,total;
} count = {0,0,0,0,0,0,0,0};

long long prev_head=0;
int quiet=0;
void *our_mmap;

static void pe_handler(int signum, siginfo_t *info, void * context) {
  int fd = info->si_fd;
  ioctl(fd, PERF_EVENT_IOC_DISABLE, 0);

  prev_head=perf_mmap_read(our_mmap,MMAP_DATA_SIZE,prev_head,
                           sample_type,read_format,NULL,quiet);

  switch(info->si_code) {
     case POLL_IN:  count.in++;  break;
     case POLL_OUT: count.out++; break;
     case POLL_MSG: count.msg++; break;
     case POLL_ERR: count.err++; break;
     case POLL_PRI: count.pri++; break;
     case POLL_HUP: count.hup++; break;
     default: count.unknown++; break;
  }

  count.total++;

  ioctl(fd, PERF_EVENT_IOC_REFRESH, 1);
}

static void
prepare_pe() {
  //Set signal stuff
  struct sigaction sa;
  memset(&sa, 0, sizeof(struct sigaction));
  sa.sa_sigaction = pe_handler;
  sa.sa_flags = SA_SIGINFO;

  if (sigaction( SIGIO, &sa, NULL) < 0) {
     fprintf(stderr,"Error setting up signal handler\n");
     exit(1);
  }
}
