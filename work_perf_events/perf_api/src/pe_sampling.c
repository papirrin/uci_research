#define _GNU_SOURCE 1

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

#include <string.h>
#include <errno.h>
#include <inttypes.h>
#include <fcntl.h>
//#include <sys/utsname.h>
//#include <sys/types.h>
#include <signal.h>

#include <sys/mman.h>
#include <sys/ioctl.h>
#include <asm/unistd.h>
#include "pe_internal.h"
#include "mb.h"


/* From PAPI */
typedef struct
{
  int group_leader_fd;            /* fd of group leader                   */
  int event_fd;                   /* fd of event                          */
  int event_opened;               /* event successfully opened            */
  uint32_t nr_mmap_pages;         /* number pages in the mmap buffer      */
  void *mmap_buf;                 /* used for control/profiling           */
  uint64_t tail;                  /* current read location in mmap buffer */
  uint64_t mask;                  /* mask used for wrapping the pages     */
  struct perf_event_attr attr;    /* perf_event config structure          */
//  unsigned int wakeup_mode;       /* wakeup mode when sampling            */
} pe_event_info_t;


typedef struct
{
  int num_events;                 /* number of events in control state */
  int cpu;                        /* which cpu to measure              */
  pid_t tid;                      /* thread we are monitoring          */
  pe_event_info_t events[PERF_EVENT_MAX_MPX_COUNTERS];
} pe_control_t;

typedef struct{
  long count;
} pe_sample_t;


/* Set the F_SETOWN_EX flag on the fd.                          */
/* This affects which thread an overflow signal gets sent to    */
/* Handled in a subroutine to handle the fact that the behavior */
/* is dependent on kernel version.                              */
static int
fcntl_setown_fd(int fd) {

   int ret;
   struct f_owner_ex fown_ex;

      /* set ownership of the descriptor */
      fown_ex.type = F_OWNER_TID;
      fown_ex.pid  = getpid();
      ret = fcntl(fd, F_SETOWN_EX, (unsigned long)&fown_ex );

      if ( ret == -1 ) {
         REPORTERROR( "cannot fcntl(F_SETOWN_EX) on %d: %s",
                    fd, strerror( errno ) );
         return PE_ESYS;
      }
   return PE_OK;
}



static int
prepare_fd(pe_control_t *ctl, int evt_idx){
  int ret;
  void *buf_addr;
  int fd = ctl->events[evt_idx].event_fd;

  /* Register that we would like a SIGIO notification when a mmap'd page */
  /* becomes full.                                                       */
  ret = fcntl( fd, F_SETFL, O_ASYNC | O_RDWR | O_NONBLOCK );
  if ( ret ) {
    REPORTERROR ( "fcntl(%d, F_SETFL, O_ASYNC | O_NONBLOCK) "
                "returned error: %s", fd, strerror( errno ) );
    return PE_ESYS;
  }

  /* Set the F_SETOWN_EX flag on the fd.                          */
  /* This affects which thread an overflow signal gets sent to.   */
   ret=fcntl_setown_fd(fd);
   if (ret!=PE_OK) return ret;

   /* Set FD_CLOEXEC.  Otherwise if we do an exec with an overflow */
   /* running, the overflow handler will continue into the exec()'d*/
   /* process and kill it because no signal handler is set up.     */
   ret=fcntl(fd, F_SETFD, FD_CLOEXEC);
   if (ret) {
      return PE_ESYS;
   }

   /* when you explicitely declare that you want a particular signal,  */
   /* even with you use the default signal, the kernel will send more  */
   /* information concerning the event to the signal handler.          */
   /*                                                                  */
   /* In particular, it will send the file descriptor from which the   */
   /* event is originating which can be quite useful when monitoring   */
   /* multiple tasks from a single thread.                             */
   ret = fcntl( fd, F_SETSIG, PE_SAMPLE_SIG );
   if ( ret == -1 ) {
      REPORTERROR( "cannot fcntl(F_SETSIG,%d) on %d: %s",
                 PE_SAMPLE_SIG, fd,
                 strerror( errno ) );
      return PE_ESYS;
   }


   /* mmap() the sample buffer */
   buf_addr = mmap( NULL, ctl->events[evt_idx].nr_mmap_pages * getpagesize(),
                    PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0 );
   if ( buf_addr == MAP_FAILED ) {
      REPORTERROR( "mmap(NULL,%d,%d,%d,%d,0): %s",
                 ctl->events[evt_idx].nr_mmap_pages * getpagesize(  ),
                 PROT_READ, MAP_SHARED, fd, strerror( errno ) );
      return ( PE_ESYS );
   }

   SUBDBG( "Sample buffer for fd %d is located at %p\n", fd, buf_addr );

   /* Set up the mmap buffer and its associated helpers */
   ctl->events[evt_idx].mmap_buf = (struct perf_counter_mmap_page *) buf_addr;
   ctl->events[evt_idx].tail = 0;
   ctl->events[evt_idx].mask = ( ctl->events[evt_idx].nr_mmap_pages - 1 ) *
                               getpagesize() - 1;

   return PE_OK;
}


/* Open all events in the control state */
static int
open_pe_events( pe_control_t *ctl )
{

   int i, ret = PE_OK;
   long pid = ctl->tid;
   

   for( i = 0; i < ctl->num_events; i++ ) {

      ctl->events[i].event_opened=0;

      /* set up the attr structure.  We don't set up all fields here */
      /* as some have already been set up previously.                */

      /* group leader (event 0) is special                */
      if ( i == 0  ) {
         ctl->events[i].attr.pinned = 1;
         ctl->events[i].attr.disabled = 1;
         ctl->events[i].group_leader_fd=-1;
      } else {
         ctl->events[i].attr.pinned=0;
         ctl->events[i].attr.disabled = 0;
         ctl->events[i].group_leader_fd=ctl->events[0].event_fd;
      }

      /* try to open */
      ctl->events[i].event_fd = sys_perf_event_open( &ctl->events[i].attr,
                                                     pid,
                                                     ctl->cpu,
                               ctl->events[i].group_leader_fd,
                                                     0 /* flags */
                                                     );

      if ( ctl->events[i].event_fd == -1 ) {
         SUBDBG("sys_perf_event_open returned error on event #%d."
                "  Error: %s\n",
                i, strerror( errno ) );
         if (errno == EPERM) ret = PE_EPERM;
         else ret = PE_ECNFLCT;
         goto open_pe_cleanup;
      }

      SUBDBG ("sys_perf_event_open: tid: %ld, cpu_num: %d,"
              " group_leader/fd: %d, event_fd: %d,"
              " read_format: 0x%"PRIu64"\n",
              pid, ctl->cpu, ctl->events[i].group_leader_fd,
              ctl->events[i].event_fd, ctl->events[i].attr.read_format);

      ctl->events[i].event_opened=1;
    }

   /* Now that we've successfully opened all of the events, do whatever  */
   /* "tune-up" is needed to attach the mmap'd buffers, signal handlers, */
   /* and so on.                                                         */
   for ( i = 0; i < ctl->num_events; i++ ) {

      /* If sampling is enabled, hook up signal handler */
      if ( ctl->events[i].attr.sample_period ) {
         ret = prepare_fd( ctl, i );
         if ( ret != PE_OK ) {
            /* All of the fds are open, so we need to clean up all of them */
            i = ctl->num_events;
            goto open_pe_cleanup;
         }
      } else {
         /* Make sure this is NULL so close_pe_events works right */
         ctl->events[i].mmap_buf = NULL;
      }
   }

   return PE_OK;

open_pe_cleanup:
   /* We encountered an error, close up the fds we successfully opened.  */
   /* We go backward in an attempt to close group leaders last, although */
   /* That's probably not strictly necessary.                            */
   while ( i > 0 ) {
      i--;
      if (ctl->events[i].event_fd>=0) {
         close( ctl->events[i].event_fd );
         ctl->events[i].event_opened=0;
      }
   }

   return ret;
}
/* Close all of the opened events */
static int
close_pe_events( pe_control_t *ctl )
{
   int i;
   int num_closed=0;
   int events_not_opened=0;

   /* Close child events first */
   for( i=0; i<ctl->num_events; i++ ) {

      if (ctl->events[i].event_opened) {

         if (ctl->events[i].group_leader_fd!=-1) {
            if ( ctl->events[i].mmap_buf ) {
               if ( munmap ( ctl->events[i].mmap_buf,
                             ctl->events[i].nr_mmap_pages * getpagesize() ) ) {
                  REPORTERROR( "munmap of fd = %d returned error: %s",
                             ctl->events[i].event_fd, strerror( errno ) );
                  return PE_ESYS;
               }
            }
           if ( close( ctl->events[i].event_fd ) ) {
               REPORTERROR( "close of fd = %d returned error: %s",
                       ctl->events[i].event_fd, strerror( errno ) );
               return PE_ESYS;
            } else {
               num_closed++;
            }
            ctl->events[i].event_opened=0;
         }
      }
      else {
        events_not_opened++;
      }
   }

   /* Close the group leaders last */
   for( i=0; i<ctl->num_events; i++ ) {

      if (ctl->events[i].event_opened) {

         if (ctl->events[i].group_leader_fd==-1) {
            if ( ctl->events[i].mmap_buf ) {
               if ( munmap ( ctl->events[i].mmap_buf,
                             ctl->events[i].nr_mmap_pages * getpagesize() ) ) {
                  REPORTERROR( "munmap of fd = %d returned error: %s",
                             ctl->events[i].event_fd, strerror( errno ) );
                  return PE_ESYS;
               }
            }


            if ( close( ctl->events[i].event_fd ) ) {
               REPORTERROR( "close of fd = %d returned error: %s",
                       ctl->events[i].event_fd, strerror( errno ) );
               return PE_ESYS;
            } else {
               num_closed++;
            }
            ctl->events[i].event_opened=0;
         }
      }
   }


   if (ctl->num_events!=num_closed) {
      if (ctl->num_events!=(num_closed+events_not_opened)) {
         REPORTERROR("Didn't close all events: "
                   "Closed %d Not Opened: %d Expected %d\n",
                   num_closed,events_not_opened,ctl->num_events);
         return PE_EBUG;
      }
   }

   ctl->num_events=0;

   return PE_OK;
}

static uint64_t
mmap_read_head( pe_event_info_t *pe )
{
   struct perf_event_mmap_page *pc = pe->mmap_buf;
   int head;

   if ( pc == NULL ) {
      REPORTERROR( "perf_event_mmap_page is NULL" );
      return 0;
   }

   head = pc->data_head;
   rmb(  );

   return head;
}

static void
mmap_write_tail( pe_event_info_t *pe, uint64_t tail )
{
   struct perf_event_mmap_page *pc = pe->mmap_buf;

   /* ensure all reads are done before we write the tail out. */
   pc->data_tail = tail;
}
