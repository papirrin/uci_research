#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include "papi_debug.h"

/** \internal 
@defgroup ret_codes Return Codes
Return Codes
All of the functions contained in the PerfAPI return standardized error codes.
Values greater than or equal to zero indicate success, less than zero indicates
failure. 
@{
*/

#define PAPI_OK          0     /**< No error */
#define PAPI_EINVAL     -1     /**< Invalid argument */
#define PAPI_ENOMEM     -2     /**< Insufficient memory */
#define PAPI_ESYS       -3     /**< A System/C library call failed */
#define PAPI_ECMP       -4     /**< Not supported by component */
#define PAPI_ESBSTR     -4     /**< Backwards compatibility */
#define PAPI_ECLOST     -5     /**< Access to the counters was lost or interrupted */
#define PAPI_EBUG       -6     /**< Internal error, please send mail to the developers */
#define PAPI_ENOEVNT    -7     /**< Event does not exist */
#define PAPI_ECNFLCT    -8     /**< Event exists, but cannot be counted due to counter resource limitations */
#define PAPI_ENOTRUN    -9     /**< EventSet is currently not running */
#define PAPI_EISRUN     -10    /**< EventSet is currently counting */
#define PAPI_ENOEVST    -11    /**< No such EventSet Available */
#define PAPI_ENOTPRESET -12    /**< Event in argument is not a valid preset */
#define PAPI_ENOCNTR    -13    /**< Hardware does not support performance counters */
#define PAPI_EMISC      -14    /**< Unknown error code */
#define PAPI_EPERM      -15    /**< Permission level does not permit operation */
#define PAPI_ENOINIT    -16    /**< PAPI hasn't been initialized yet */
#define PAPI_ENOCMP     -17    /**< Component Index isn't set */
#define PAPI_ENOSUPP    -18    /**< Not supported */
#define PAPI_ENOIMPL    -19    /**< Not implemented */
#define PAPI_EBUF       -20    /**< Buffer size exceeded */
#define PAPI_EINVAL_DOM -21    /**< EventSet domain is not supported for the operation */
#define PAPI_EATTR              -22    /**< Invalid or missing event attributes */
#define PAPI_ECOUNT             -23    /**< Too many events or attributes */
#define PAPI_ECOMBO             -24    /**< Bad combination of features */
#define PAPI_NUM_ERRORS  25    /**< Number of error messages specified in this API */

#define PAPI_NOT_INITED         0
#define PAPI_LOW_LEVEL_INITED   1       /* Low level has called library init */
#define PAPI_HIGH_LEVEL_INITED  2       /* High level has called library init */
#define PAPI_THREAD_LEVEL_INITED 4      /* Threads have been inited */
/** @} */

/** @internal  
        @defgroup domain_defns Domain definitions 
        @{ */

#define PAPI_DOM_USER    0x1    /**< User context counted */
#define PAPI_DOM_MIN     PAPI_DOM_USER
#define PAPI_DOM_KERNEL  0x2    /**< Kernel/OS context counted */
#define PAPI_DOM_OTHER   0x4    /**< Exception/transient mode (like user TLB misses ) */
#define PAPI_DOM_SUPERVISOR 0x8 /**< Supervisor/hypervisor context counted */
#define PAPI_DOM_ALL     (PAPI_DOM_USER|PAPI_DOM_KERNEL|PAPI_DOM_OTHER|PAPI_DOM_SUPERVISOR) /**< All contexts counted */
/* #define PAPI_DOM_DEFAULT PAPI_DOM_USER NOW DEFINED BY COMPONENT */
#define PAPI_DOM_MAX     PAPI_DOM_ALL
#define PAPI_DOM_HWSPEC  0x80000000     /**< Flag that indicates we are not reading CPU like stuff.
                                           The lower 31 bits can be decoded by the component into something
                                           meaningful. i.e. SGI HUB counters */
/** @} */

/** @internal 
 *      @defgroup granularity_defns Granularity definitions 
 *      @{ */

#define PAPI_GRN_THR     0x1    /**< PAPI counters for each individual thread */
#define PAPI_GRN_MIN     PAPI_GRN_THR
#define PAPI_GRN_PROC    0x2    /**< PAPI counters for each individual process */
#define PAPI_GRN_PROCG   0x4    /**< PAPI counters for each individual process group */
#define PAPI_GRN_SYS     0x8    /**< PAPI counters for the current CPU, are you bound? */
#define PAPI_GRN_SYS_CPU 0x10   /**< PAPI counters for all CPUs individually */
#define PAPI_GRN_MAX     PAPI_GRN_SYS_CPU
/** @} */

/** @internal 
        @defgroup evt_states States of an EventSet 
        @{ */
#define PAPI_STOPPED      0x01  /**< EventSet stopped */
#define PAPI_RUNNING      0x02  /**< EventSet running */
#define PAPI_PAUSED       0x04  /**< EventSet temp. disabled by the library */
#define PAPI_NOT_INIT     0x08  /**< EventSet defined, but not initialized */
#define PAPI_OVERFLOWING  0x10  /**< EventSet has overflowing enabled */
#define PAPI_PROFILING    0x20  /**< EventSet has profiling enabled */
#define PAPI_MULTIPLEXING 0x40  /**< EventSet has multiplexing enabled */
#define PAPI_ATTACHED     0x80  /**< EventSet is attached to another thread/process */
#define PAPI_CPU_ATTACHED 0x100 /**< EventSet is attached to a specific cpu (not counting thread of execution) */
/** @} */

#define __STDC_FORMAT_MACROS
#include <inttypes.h>

void PAPIERROR(const char *format, ...){
  va_list argptr;
  va_start(argptr, format);
  vprintf(format, argptr);
  va_end(argptr);
}

static inline pid_t
mygettid( void )
{
#ifdef SYS_gettid
        return syscall( SYS_gettid );
#elif defined(__NR_gettid)
        return syscall( __NR_gettid );
#else
#error "cannot find gettid"
#endif
}


// From papi/linux_commons
#ifndef F_SETOWN_EX
   #define F_SETOWN_EX     15
   #define F_GETOWN_EX     16

   #define F_OWNER_TID     0
   #define F_OWNER_PID     1
   #define F_OWNER_PGRP    2

   struct f_owner_ex {
              int     type;
              pid_t   pid;
   };
#endif

