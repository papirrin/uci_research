/*
* File:    perf_events.c
*
* Author:  David Carrillo-Cisneros
*          carrild1@uci.edu
*          - based upon perf_events written by -
*          Corey Ashford */


// Included in papi/perf_events.c
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <syscall.h>
#include <sys/utsname.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

// In other files
#include <unistd.h>

/*
#include <stdlib.h>
#include <stdint.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <syscall.h>
#include <sys/utsname.h>

#include <sys/mman.h>
#include <sys/ioctl.h>

#include <sys/types.h>

#include <asm/unistd.h>

#include <perfmon/pfmlib.h>

#include <linux/fcntl.h>
*/
#include <linux/perf_event.h>

#include "pe_internal.h"
#include "sys_perf_event_open.c"
#include "mb.h"

/* Various definitions */

/* This is arbitrary.  Typically you can add up to ~1000 before */
/* you run out of fds                                           */
#define PERF_EVENT_MAX_MPX_COUNTERS 64

/* We really don't need fancy definitions for these */

typedef struct
{
  int group_leader_fd;            /* fd of group leader                   */
  int event_fd;                   /* fd of event                          */
  int event_opened;               /* event successfully opened            */
  uint32_t nr_mmap_pages;	  /* number pages in the mmap buffer      */
  void *mmap_buf;		  /* used for control/profiling           */
  uint64_t tail;		  /* current read location in mmap buffer */
  uint64_t mask;		  /* mask used for wrapping the pages     */
  struct perf_event_attr attr;    /* perf_event config structure          */
  unsigned int wakeup_mode;       /* wakeup mode when sampling            */
} pe_event_info_t;

typedef struct
{
  int num_events;                 /* number of events in control state */
  unsigned int domain;            /* control-state wide domain         */
  unsigned int granularity;       /* granularity                       */
  unsigned int multiplexed;       /* multiplexing enable               */
  unsigned int overflow;          /* overflow enable                   */
  unsigned int inherit;           /* inherit enable                    */
  int cpu;                        /* which cpu to measure              */
  pid_t tid;                      /* thread we are monitoring          */
  pe_event_info_t events[PERF_EVENT_MAX_MPX_COUNTERS];
  long long counts[PERF_EVENT_MAX_MPX_COUNTERS];
} pe_control_t;

typedef struct
{
  int initialized;                /* are we initialized?           */
  int state;                      /* are we opened and/or running? */
} pe_context_t;

/* These sentinels tell papi_pe_set_overflow() how to set the */
/* wakeup_events field in the event descriptor record.        */

#define WAKEUP_COUNTER_OVERFLOW 0
#define WAKEUP_PROFILING -1

#define WAKEUP_MODE_COUNTER_OVERFLOW 0
#define WAKEUP_MODE_PROFILING 1

/* Defines for ctx->state */
#define PERF_EVENTS_OPENED  0x01
#define PERF_EVENTS_RUNNING 0x02

/* Static globals */
static int nmi_watchdog_active;

/* Set the F_SETOWN_EX flag on the fd.                          */
/* This affects which thread an overflow signal gets sent to    */
/* Handled in a subroutine to handle the fact that the behavior */
/* is dependent on kernel version.                              */
static int 
fcntl_setown_fd(int fd) {

   int ret;
   struct f_owner_ex fown_ex;

      /* F_SETOWN_EX is not available until 2.6.32 */
   if(0) {
//   if (_papi_os_info.os_version < LINUX_VERSION(2,6,32)) {
	   
      /* get ownership of the descriptor */
      ret = fcntl( fd, F_SETOWN, mygettid(  ) );
      if ( ret == -1 ) {
	 PAPIERROR( "cannot fcntl(F_SETOWN) on %d: %s", fd, strerror(errno) );
	 return PAPI_ESYS;
      }
   }
   else {
      /* set ownership of the descriptor */   
      fown_ex.type = F_OWNER_TID;
      fown_ex.pid  = mygettid();
      ret = fcntl(fd, F_SETOWN_EX, (unsigned long)&fown_ex );
   
      if ( ret == -1 ) {
	 PAPIERROR( "cannot fcntl(F_SETOWN_EX) on %d: %s", 
		    fd, strerror( errno ) );
	 return PAPI_ESYS;
      }
   }
   return PAPI_OK;
}

/* multiple places.                                                 */
static unsigned int
get_read_format( unsigned int multiplex, 
		 unsigned int inherit, 
		 int format_group )
{
   unsigned int format = 0;

   /* if we need read format options for multiplexing, add them now */
   if (multiplex) {
      format |= PERF_FORMAT_TOTAL_TIME_ENABLED;
      format |= PERF_FORMAT_TOTAL_TIME_RUNNING;
   }

   if (format_group) {
     format |= PERF_FORMAT_GROUP;
   }

   SUBDBG("multiplex: %d, inherit: %d, group_leader: %d, format: 0x%x\n",
	  multiplex, inherit, format_group, format);

   return format;
}

/* The kernel developers say to never use a refresh value of 0        */
/* See https://lkml.org/lkml/2011/5/24/172                            */
/* However, on some platforms (like Power) a value of 1 does not work */
/* We're still tracking down why this happens.                        */

#if defined(__powerpc__)
#define PAPI_REFRESH_VALUE 0
#else
#define PAPI_REFRESH_VALUE 1
#endif

/********* End Kernel-version Dependent Routines  ****************/


/** Check if the current set of options is supported by  */
/*  perf_events.                                         */
/*  We do this by temporarily opening an event with the  */
/*  desired options then closing it again.  We use the   */
/*  PERF_COUNT_HW_INSTRUCTION event as a dummy event     */
/*  on the assumption it is available on all             */
/*  platforms.                                           */

static int
check_permissions( unsigned long tid, 
		   unsigned int cpu_num, 
		   unsigned int domain, 
		   unsigned int granularity,
		   unsigned int multiplex, 
		   unsigned int inherit )
{
   int ev_fd;
   struct perf_event_attr attr;

   long pid;

   /* clearing this will set a type of hardware and to count all domains */
   memset(&attr, '\0', sizeof(attr));
   attr.read_format = get_read_format(multiplex, inherit, 1);

   /* set the event id (config field) to instructios */
   /* (an event that should always exist)            */
   /* This was cycles but that is missing on Niagara */
   attr.config = PERF_COUNT_HW_INSTRUCTIONS;
	
   /* now set up domains this event set will be counting */
   if (!(domain & PAPI_DOM_SUPERVISOR)) {
      attr.exclude_hv = 1;
   }
   if (!(domain & PAPI_DOM_USER)) {
      attr.exclude_user = 1;
   }
   if (!(domain & PAPI_DOM_KERNEL)) {
      attr.exclude_kernel = 1;
   }

   if (granularity==PAPI_GRN_SYS) {
      pid = -1;
   } else {
      pid = tid;
   }

   SUBDBG("Calling sys_perf_event_open() from check_permissions\n");

   ev_fd = sys_perf_event_open( &attr, pid, cpu_num, -1, 0 );
   if ( ev_fd == -1 ) {
      SUBDBG("sys_perf_event_open returned error.  Linux says, %s", 
	     strerror( errno ) );
      return PAPI_EPERM;
   }
	
   /* now close it, this was just to make sure we have permissions */
   /* to set these options                                         */
   close(ev_fd);
   return PAPI_OK;
}



/* Maximum size we ever expect to read from a perf_event fd   */
/*  (this is the number of 64-bit values)                     */
/* We use this to size the read buffers                       */
/* The three is for event count, time_enabled, time_running   */
/*  and the counter term is count value and count id for each */
/*  possible counter value.                                   */
#define READ_BUFFER_SIZE (3 + (2 * PERF_EVENT_MAX_MPX_COUNTERS))



/* KERNEL_CHECKS_SCHEDUABILITY_UPON_OPEN is a work-around for kernel arch */
/* implementations (e.g. x86 before 2.6.33) which don't do a static event */
/* scheduability check in sys_perf_event_open.  It is also needed if the  */
/* kernel is stealing an event, such as when NMI watchdog is enabled.     */

static int
check_scheduability( pe_context_t *ctx, pe_control_t *ctl, int idx )
{
   int retval = 0, cnt = -1;
   ( void ) ctx;			 /*unused */
   long long papi_pe_buffer[READ_BUFFER_SIZE];
   int i,group_leader_fd;

//   if (bug_check_scheduability()) {
     if (0) {

      /* If the kernel isn't tracking scheduability right       */
      /* Then we need to start/stop/read to force the event     */
      /* to be scheduled and see if an error condition happens. */

      /* get the proper fd to start */
      group_leader_fd=ctl->events[idx].group_leader_fd;
      if (group_leader_fd==-1) group_leader_fd=ctl->events[idx].event_fd;

      /* start the event */
      retval = ioctl( group_leader_fd, PERF_EVENT_IOC_ENABLE, NULL );
      if (retval == -1) {
	 PAPIERROR("ioctl(PERF_EVENT_IOC_ENABLE) failed.\n");
	 return PAPI_ESYS;
      }

      /* stop the event */
      retval = ioctl(group_leader_fd, PERF_EVENT_IOC_DISABLE, NULL );
      if (retval == -1) {
	 PAPIERROR( "ioctl(PERF_EVENT_IOC_DISABLE) failed.\n" );
	 return PAPI_ESYS;
      }

      /* See if a read returns any results */
      cnt = read( group_leader_fd, papi_pe_buffer, sizeof(papi_pe_buffer));
      if ( cnt == -1 ) {
	 SUBDBG( "read returned an error!  Should never happen.\n" );
	 return PAPI_ESYS;
      }

      if ( cnt == 0 ) {
         /* We read 0 bytes if we could not schedule the event */
         /* The kernel should have detected this at open       */
         /* but various bugs (including NMI watchdog)          */
         /* result in this behavior                            */

	 return PAPI_ECNFLCT;

     } else {

	/* Reset all of the counters (opened so far) back to zero      */
	/* from the above brief enable/disable call pair.              */

	/* We have to reset all events because reset of group leader      */
        /* does not reset all.                                            */
	/* we assume that the events are being added one by one and that  */
        /* we do not need to reset higher events (doing so may reset ones */
        /* that have not been initialized yet.                            */

	/* Note... PERF_EVENT_IOC_RESET does not reset time running       */
	/* info if multiplexing, so we should avoid coming here if        */
	/* we are multiplexing the event.                                 */
        for( i = 0; i < idx; i++) {
	   retval=ioctl( ctl->events[i].event_fd, PERF_EVENT_IOC_RESET, NULL );
	   if (retval == -1) {
	      PAPIERROR( "ioctl(PERF_EVENT_IOC_RESET) #%d/%d %d "
			 "(fd %d)failed.\n",
			 i,ctl->num_events,idx,ctl->events[i].event_fd);
	      return PAPI_ESYS;
	   }
	}
      }
   }
   return PAPI_OK;
}


/* Do some extrta work on a perf_event fd if we're doing sampling */
/* This mostly means setting up the mmap buffer.                  */
static int
tune_up_fd( pe_control_t *ctl, int evt_idx )
{
   int ret;
   void *buf_addr;
   int fd = ctl->events[evt_idx].event_fd;

   /* Register that we would like a SIGIO notification when a mmap'd page */
   /* becomes full.                                                       */
   ret = fcntl( fd, F_SETFL, O_ASYNC | O_NONBLOCK );
   if ( ret ) {
      PAPIERROR ( "fcntl(%d, F_SETFL, O_ASYNC | O_NONBLOCK) "
		  "returned error: %s", fd, strerror( errno ) );
      return PAPI_ESYS;
   }

   /* Set the F_SETOWN_EX flag on the fd.                          */
   /* This affects which thread an overflow signal gets sent to.   */
   ret=fcntl_setown_fd(fd);
   if (ret!=PAPI_OK) return ret;
	   
   /* Set FD_CLOEXEC.  Otherwise if we do an exec with an overflow */
   /* running, the overflow handler will continue into the exec()'d*/
   /* process and kill it because no signal handler is set up.     */
   ret=fcntl(fd, F_SETFD, FD_CLOEXEC);
   if (ret) {
      return PAPI_ESYS;
   }

   /* when you explicitely declare that you want a particular signal,  */
   /* even with you use the default signal, the kernel will send more  */
   /* information concerning the event to the signal handler.          */
   /*                                                                  */
   /* In particular, it will send the file descriptor from which the   */
   /* event is originating which can be quite useful when monitoring   */
   /* multiple tasks from a single thread.                             */
/*
   ret = fcntl( fd, F_SETSIG, _papi_pe_vector.cmp_info.hardware_intr_sig );
   if ( ret == -1 ) {
      PAPIERROR( "cannot fcntl(F_SETSIG,%d) on %d: %s",
		 _papi_pe_vector.cmp_info.hardware_intr_sig, fd,
		 strerror( errno ) );
      return PAPI_ESYS;
   }
*/
   /* mmap() the sample buffer */
   buf_addr = mmap( NULL, ctl->events[evt_idx].nr_mmap_pages * getpagesize(),
		    PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0 );
   if ( buf_addr == MAP_FAILED ) {
      PAPIERROR( "mmap(NULL,%d,%d,%d,%d,0): %s",
		 ctl->events[evt_idx].nr_mmap_pages * getpagesize(  ), 
		 PROT_READ, MAP_SHARED, fd, strerror( errno ) );
      return ( PAPI_ESYS );
   }

   SUBDBG( "Sample buffer for fd %d is located at %p\n", fd, buf_addr );

   /* Set up the mmap buffer and its associated helpers */
   ctl->events[evt_idx].mmap_buf = (struct perf_counter_mmap_page *) buf_addr;
   ctl->events[evt_idx].tail = 0;
   ctl->events[evt_idx].mask = ( ctl->events[evt_idx].nr_mmap_pages - 1 ) * 
                               getpagesize() - 1;

   return PAPI_OK;
}


/* Open all events in the control state */
static int
open_pe_events( pe_context_t *ctx, pe_control_t *ctl )
{

   int i, ret = PAPI_OK;
   long pid;

   if (ctl->granularity==PAPI_GRN_SYS) {
      pid = -1;
   }
   else {
      pid = ctl->tid;
   }

   for( i = 0; i < ctl->num_events; i++ ) {

      ctl->events[i].event_opened=0;

      /* set up the attr structure.  We don't set up all fields here */
      /* as some have already been set up previously.                */

      /* group leader (event 0) is special                */
      /* If we're multiplexed, everyone is a group leader */
      if (( i == 0 ) || (ctl->multiplexed)) {
         ctl->events[i].attr.pinned = !ctl->multiplexed;
	 ctl->events[i].attr.disabled = 1;
	 ctl->events[i].group_leader_fd=-1;
         ctl->events[i].attr.read_format = get_read_format(ctl->multiplexed, 
							   ctl->inherit, 
							   !ctl->multiplexed );
      } else {
	 ctl->events[i].attr.pinned=0;
	 ctl->events[i].attr.disabled = 0;
	 ctl->events[i].group_leader_fd=ctl->events[0].event_fd;
         ctl->events[i].attr.read_format = get_read_format(ctl->multiplexed, 
							   ctl->inherit, 
							   0 );
      }


      /* try to open */
      ctl->events[i].event_fd = sys_perf_event_open( &ctl->events[i].attr, 
						     pid,
						     ctl->cpu,
			       ctl->events[i].group_leader_fd,
						     0 /* flags */
						     );
      
      if ( ctl->events[i].event_fd == -1 ) {
	 SUBDBG("sys_perf_event_open returned error on event #%d."
		"  Error: %s\n",
		i, strerror( errno ) );
	 if (errno == EPERM) ret = PAPI_EPERM;
	 else ret = PAPI_ECNFLCT;
	 goto open_pe_cleanup;
      }

      SUBDBG ("sys_perf_event_open: tid: %ld, cpu_num: %d,"
              " group_leader/fd: %d, event_fd: %d,"
              " read_format: 0x%"PRIu64"\n",
	      pid, ctl->cpu, ctl->events[i].group_leader_fd, 
	      ctl->events[i].event_fd, ctl->events[i].attr.read_format);


      /* in many situations the kernel will indicate we opened fine */
      /* yet things will fail later.  So we need to double check    */
      /* we actually can use the events we've set up.               */

      /* This is not necessary if we are multiplexing, and in fact */
      /* we cannot do this properly if multiplexed because         */
      /* PERF_EVENT_IOC_RESET does not reset the time running info */
      if (!ctl->multiplexed) {
	 ret = check_scheduability( ctx, ctl, i );

         if ( ret != PAPI_OK ) {
	    /* the last event did open, so we need to bump the counter */
	    /* before doing the cleanup                                */
	    i++;
		                          
            goto open_pe_cleanup;
	 }
      }
      ctl->events[i].event_opened=1;
   }

   /* Now that we've successfully opened all of the events, do whatever  */
   /* "tune-up" is needed to attach the mmap'd buffers, signal handlers, */
   /* and so on.                                                         */
   for ( i = 0; i < ctl->num_events; i++ ) {

      /* If sampling is enabled, hook up signal handler */
      if ( ctl->events[i].attr.sample_period ) {
	 ret = tune_up_fd( ctl, i );
	 if ( ret != PAPI_OK ) {
	    /* All of the fds are open, so we need to clean up all of them */
	    i = ctl->num_events;
	    goto open_pe_cleanup;
	 }
      } else {
	 /* Make sure this is NULL so close_pe_events works right */
	 ctl->events[i].mmap_buf = NULL;
      }
   }

   /* Set num_evts only if completely successful */
   ctx->state |= PERF_EVENTS_OPENED;
		
   return PAPI_OK;

open_pe_cleanup:
   /* We encountered an error, close up the fds we successfully opened.  */
   /* We go backward in an attempt to close group leaders last, although */
   /* That's probably not strictly necessary.                            */
   while ( i > 0 ) {
      i--;
      if (ctl->events[i].event_fd>=0) {
	 close( ctl->events[i].event_fd );
	 ctl->events[i].event_opened=0;
      }
   }

   return ret;
}

/* Close all of the opened events */
static int
close_pe_events( pe_context_t *ctx, pe_control_t *ctl )
{
   int i;
   int num_closed=0;
   int events_not_opened=0;

   /* should this be a more serious error? */
   if ( ctx->state & PERF_EVENTS_RUNNING ) {
      SUBDBG("Closing without stopping first\n");
   }

   /* Close child events first */
   for( i=0; i<ctl->num_events; i++ ) {

      if (ctl->events[i].event_opened) {

         if (ctl->events[i].group_leader_fd!=-1) {
            if ( ctl->events[i].mmap_buf ) {
	       if ( munmap ( ctl->events[i].mmap_buf,
		             ctl->events[i].nr_mmap_pages * getpagesize() ) ) {
	          PAPIERROR( "munmap of fd = %d returned error: %s",
			     ctl->events[i].event_fd, strerror( errno ) );
	          return PAPI_ESYS;
	       }
	    }

            if ( close( ctl->events[i].event_fd ) ) {
	       PAPIERROR( "close of fd = %d returned error: %s",
		       ctl->events[i].event_fd, strerror( errno ) );
	       return PAPI_ESYS;
	    } else {
	       num_closed++;
	    }
	    ctl->events[i].event_opened=0;
	 }
      }
      else {
	events_not_opened++;
      }
   }

   /* Close the group leaders last */
   for( i=0; i<ctl->num_events; i++ ) {

      if (ctl->events[i].event_opened) {

         if (ctl->events[i].group_leader_fd==-1) {
            if ( ctl->events[i].mmap_buf ) {
	       if ( munmap ( ctl->events[i].mmap_buf,
		             ctl->events[i].nr_mmap_pages * getpagesize() ) ) {
	          PAPIERROR( "munmap of fd = %d returned error: %s",
			     ctl->events[i].event_fd, strerror( errno ) );
	          return PAPI_ESYS;
	       }
	    }


            if ( close( ctl->events[i].event_fd ) ) {
	       PAPIERROR( "close of fd = %d returned error: %s",
		       ctl->events[i].event_fd, strerror( errno ) );
	       return PAPI_ESYS;
	    } else {
	       num_closed++;
	    }
	    ctl->events[i].event_opened=0;
	 }
      }
   }


   if (ctl->num_events!=num_closed) {
      if (ctl->num_events!=(num_closed+events_not_opened)) {
         PAPIERROR("Didn't close all events: "
		   "Closed %d Not Opened: %d Expected %d\n",
		   num_closed,events_not_opened,ctl->num_events);
         return PAPI_EBUG;
      }
   }

   ctl->num_events=0;

   ctx->state &= ~PERF_EVENTS_OPENED;

   return PAPI_OK;
}

/* These functions are based on builtin-record.c in the  */
/* kernel's tools/perf directory.                        */

static uint64_t
mmap_read_head( pe_event_info_t *pe )
{
   struct perf_event_mmap_page *pc = pe->mmap_buf;
   int head;

   if ( pc == NULL ) {
      PAPIERROR( "perf_event_mmap_page is NULL" );
      return 0;
   }

   head = pc->data_head;
   rmb(  );

   return head;
}

static void
mmap_write_tail( pe_event_info_t *pe, uint64_t tail )
{
   struct perf_event_mmap_page *pc = pe->mmap_buf;

   /* ensure all reads are done before we write the tail out. */
   pc->data_tail = tail;
}

/* Does the kernel define these somewhere? */
struct ip_event {
   struct perf_event_header header;
   uint64_t ip;
};
struct lost_event {
   struct perf_event_header header;
   uint64_t id;
   uint64_t lost;
};
typedef union event_union {
   struct perf_event_header header;
   struct ip_event ip;
   struct lost_event lost;
} perf_sample_event_t;

